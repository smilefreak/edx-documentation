.. 2016 index file for date based release notes pages

####################################
edX Release Notes 2016
####################################

The following pages summarize what is new in 2016.


.. toctree::
   :maxdepth: 1


   01-11-2016
   01-06-2016

