####################################
edX Release Notes
####################################

.. toctree::
   :maxdepth: 2

   front_matter/index

.. There is no coming soon information in the 2016-01-11 notes.
.. Remove coming_soon.rst from the exclude_patterns array in
.. conf.py when re-enabling this toctree.
.. .. toctree::
..   :maxdepth: 2

..   coming_soon.rst


******************
What's New?
******************

See released changes for a particular edX :ref:`product by date<By Product>`,
or by :ref:`date across products<By Date>`.

.. _By Date:

==================
By Date
==================

+++++++
2016
+++++++

.. toctree::
   :maxdepth: 1

   2016/index

+++++++++++++++++
2015 and earlier
+++++++++++++++++

.. toctree::
   :maxdepth: 1

   2015/index
   2014/index

.. _By Product:

==================
By Product
==================

.. toctree::
   :maxdepth: 1

   analytics_index
   discussions_index
   doc_index
   insights_index
   lms_index
   mobile_index
   openedx_index
   ORA_index
   studio_index
   website_index
   xblocks_index



.. include:: reusables/documentation.rst


.. include:: ../../links/links.rst
