.. _edX Learner's Guide:

###################
EdX Learner's Guide
###################

.. toctree::
   :numbered:
   :maxdepth: 2

   front_matter/index
   SFD_introduction
   SFD_account
   sfd_dashboard_profile/index
   SFD_enrolling
   SFD_credit_courses/index
   SFD_licensing
   SFD_video_player
   SFD_bookmarks
   SFD_google_docs
   SFD_certificates
   SFD_mobile
   SFD_mathformatting
   SFD_ORA
   sfd_discussions/index
   SFD_wiki
   timed_exams
