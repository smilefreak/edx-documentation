.. _Creating a New Course:

###########################
Creating a New Course
###########################

This topic describes how to create a course with Studio.

.. contents::
  :local:
  :depth: 1

Another way to create a course is to re-run an existing course. See
:ref:`Rerun a Course`.

You can also :ref:`Export a Course` and :ref:`Import a Course` through Studio.
You can do this when you need to edit the course in XML.


.. _Create a New Course:

*******************
Create a New Course
*******************

#. Log in to Studio.
#. Select **New Course**.
#. Enter course information as needed and select **Create**.

   .. note::  Enter new course information carefully. This information becomes
      part of the URL for your course. As part of your course URL, the total
      number of characters in the following four fields must be 65 or fewer.

   .. image:: ../../../shared/images/new_course_info.png
      :width: 600
      :alt: Image of the course creation page in Studio.

   * For **Course Name**, enter the title of your course. For example, the name
     may be "Sets, Maps, and Symmetry Groups". Use title capitalization for the
     course title.

   * For **Organization**, enter the identifier for your organization. Do not
     include spaces or special characters.


   * For **Course Number**, enter both a subject abbreviation and a number. For
     example, for public health course number 207, enter **PH207**. For math
     course 101x, enter **Math101x**. Do not include spaces or special
     characters in the course number.

     .. note:: If your course will be open to the world, be sure to include the
        "x". If it is exclusively an on-campus offering, do not include the "x".

   * For **Course Run**, enter the term in which your course will run. For
     example, enter 2014SOND or T2_2014. Do not include spaces or special
     characters.

     The value that you enter for the run does not affect the course start date
     that you define for the course. For more information, see :ref:`Scheduling
     Your Course`.

   .. only:: Partners

     .. note::
       For courses on edx.org and edX Edge, to change the URL after the course
       is created, you must contact edX through the `Partner Portal`_.

#. Select **Save.**

You then see the empty course outline.

.. _Edit Your Course:

************************
Edit Your Course
************************

After you create a course, the course opens in Studio automatically and you
can begin editing.

When you return to Studio later, the Studio **My Courses** dashboard page lists
the courses that you created as well as any courses for which you have course
team privileges.

.. image:: ../../../shared/images/open_course.png
  :width: 600
  :alt: Image of the course on the Studio dashboard

To open a course, select the course name. The Studio **Course Outline** page
appears.


.. _Partner Portal: https://partners.edx.org/edx_zendesk

